package assignment1

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

//Global const
const ApiRoot = "http://api.gbif.org/v1/"
const SpeciesRoot = "http://api.gbif.org/v1/occurrence/search?country="
const CountryRoot = "https://restcountries.eu/rest/v2/alpha/"

//Global time for calculating the running time of the program
var totalTime = time.Now().Unix()

//Global JSON structs
type Diagn struct {
	Gbif          int  `json:"gbif"`
	RestCountries int  `json:"restCountries"`
	Version       string  `json:"version"`
	Uptime        int64 `json:"uptime"`
}

type Country struct {
	Code        string `json:"code"`
	CountryName string `json:"name"`
	CountryFlag string `json:"flag"`
	Species     []string `json:"Species"`
	SpeciesKey  []int    `json:"speciesKey"`
}

type Specie struct {
	Key            int    `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Order          string `json:"order"`
	Family         string `json:"family"`
	Genus          string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year           string `json:"year"`
}

//Struct for finding the year in the Species struct
type SpeciesYear struct {
	Year			string `json:"year"`
	BracketYear 	string `json:"bracketYear"`
}

//Default handler
func HandlerDefault(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprint(w, "You are now on the default page")
}

// Handler for Country
func HandlerCountry(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "not implemented yet", http.StatusNotImplemented)
		return
	}
	http.Header.Add(w.Header(), "content-type", "application/json")
	parts := strings.Split(r.URL.Path, "/")
	// error handling
	if len(parts) != 5 {
		http.Error(w, "Malformed URL", http.StatusBadRequest)
		return
	}

	url := CountryRoot + parts[4]
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	nameUrl := SpeciesRoot + parts[4]

	defer res.Body.Close()

	var countries Country
	countries.Code = parts[4]
	err = json.NewDecoder(res.Body).Decode(&countries)
	if err != nil {
		fmt.Println("Error", err)
	}

	// Limit
	partsLimit := strings.Split(r.URL.RequestURI(), "limit=")

	var resSpecies *http.Response

	//Split the URL with limit.
	if len(partsLimit) == 2 {
		resSpecies, err = http.Get(nameUrl + "&limit=" + partsLimit[1])
		if err != nil {
			log.Fatal(err)
		}
	} else {
		resSpecies, err = http.Get(nameUrl)
		if err != nil {
			log.Fatal(err)
		}
	}

	temp := struct {
		Result []map[string]interface{} `json:"results"`
	}{}

	err = json.NewDecoder(resSpecies.Body).Decode(&temp)
	if err != nil {
		fmt.Println("Error", err)
	}

	tempMap := map[int]string{}
	for i, _ := range temp.Result {
		tempMap[int(temp.Result[i]["speciesKey"].(float64))]=temp.Result[i]["species"].(string)
	}
	for k, v := range tempMap {
		countries.SpeciesKey = append(countries.SpeciesKey, k)
		countries.Species = append(countries.Species, v)
	}

	w.Header().Add("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(countries)
	if err != nil {
		fmt.Println("Error", err)
	}
}

//Handler for finding species
func HandlerSpecies(w http.ResponseWriter, r *http.Request) {
	//Checks that it is GET that is used
	if r.Method != http.MethodGet {
		http.Error(w, "not implemented yet", http.StatusNotImplemented)
		return
	}
	http.Header.Add(w.Header(), "content-type", "application/json")
	//Splits the url by /
	parts := strings.Split(r.URL.Path, "/")
	// Error handling
	if len(parts) != 5 {
		http.Error(w, "Malformed URL", http.StatusBadRequest)
		return
	}

	// Creates url to get the specific specie
	url := ApiRoot + "species/" + parts[4]
	// GET url, gives us a response and error value
	res, err := http.Get(url)
	//Error
	if err != nil {
		log.Fatal(err)
	}


	var species Specie
	// Returns a new decoder that reads from r and
	err = json.NewDecoder(res.Body).Decode(&species)
	if err != nil {
		fmt.Println("Error", err)
	}

	url2 := ApiRoot + "species/" + parts[4] + "/name"
	rep, err := http.Get(url2)
	if err != nil {
		log.Fatal(err)
	}

	var Year SpeciesYear

	err = json.NewDecoder(rep.Body).Decode(&Year)
	if err != nil {
		fmt.Println("Error", err)
	}

	if Year.Year == "" {
		species.Year = Year.BracketYear
	} else {
		species.Year = Year.Year
	}

	err = json.NewEncoder(w).Encode(species)
	if err != nil {
		fmt.Println("Error", err)
	}
}

func HandlerDiag(w http.ResponseWriter, r *http.Request) {
	gbifResp, err := http.Get(ApiRoot)
	if err != nil {
		fmt.Println("Error", err)
		return
	}

	countryResp, err := http.Get("https://restcountries.eu/rest/v2")
	if err != nil {
		log.Fatalln(err)
	}

	gbifStatus := gbifResp.StatusCode
	countryState := countryResp.StatusCode

	t := time.Now().Unix()

	diagOut := Diagn{gbifStatus, countryState, "v1", t - totalTime }

	err = json.NewEncoder(w).Encode(diagOut)
	if err != nil {
		fmt.Println("Error", err)
	}
}