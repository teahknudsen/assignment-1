package main

import (
	"assignment1"
	"log"
	"net/http"
	"os"
)

func main() {
	//Set port to 8080 if port is not provided by Heroku
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	//Handlerfunctions
	http.HandleFunc("/", assignment1.HandlerDefault)
	http.HandleFunc("/conservation/v1/country/", assignment1.HandlerCountry)
	http.HandleFunc("/conservation/v1/species/", assignment1.HandlerSpecies)
	http.HandleFunc("/conservation/v1/diag/", assignment1.HandlerDiag)
	log.Fatal(http.ListenAndServe(":" + port, nil))
}

